<?php

// Función encargada de formatear campos de una consulta combinada,
// nos ahorrará trabajo al no tener que estar usando alias en las consultas
function mysqli_fetch_array_esdi($rs) {
	$array_formateada = array();
	for ($i = 0; $i < mysqli_num_fields($rs); ++$i) {
		$info = mysqli_fetch_field_direct($rs, $i);
		array_push($array_formateada, "$info->table.$info->name");
		//echo $qualified_names[$i]." ";
	}

	if ($fila = mysqli_fetch_row($rs)) {
		$fila = array_combine($array_formateada, $fila);
	}
	return $fila;
}

$db_host="localhost";
$db_nomBBDD="usabilidad";
$db_usuari="root";
$db_contrasenya="";
$connexio="";
$connexio= mysqli_connect ($db_host,$db_usuari,$db_contrasenya,$db_nomBBDD);
if (mysqli_connect_errno()) {
		echo "No se pudo conectar a la BBDD: " . mysqli_connect_errno();
		exit ();
 }
mysqli_set_charset($connexio, "utf8");
?>

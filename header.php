<?php



?>
<!DOCTYPE html>
<html>
<head>
<title>Alumni Network</title>
<meta charset="UTF-8" />
		<meta name="description" content="Práctica Usabilidad M7 2n DAM" />
		<meta name="keywords" content="html, css, php" />
		<meta name="author" content="Jordi Palomar" />
		<link rel="stylesheet" type="text/css" href="search.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="post.css">
		<script src="js/jquery-3.1.1.min.js"></script>
</head>
<body>
<div class="cabeceramaestra">
	<div class="cabecera">
	<a href="index.php"><img src="img/logo.png" class="logo" alt="Alumni Network" /></a>
	<h2>Lugar de encuentro de exalumnos</h2>
	</div>
	<div class="cabecera2">
	
	<?php 
	include('search.php');
	?>
	
	</div>
</div>
<div class="menu">
	
	<a href="index.php" class="menu">Home</a>
	<a href="index2.php" class="menu">Web No Usable</a>
	<a href="index.php?info" class="menu">¿Qué somos?</a>
	<?php
	if (!isset($_SESSION["usuario"])){
		echo '<a href="index.php?register" class="menu">Registrarse</a>';
	}else{
		echo '<a href="index.php?logout" class="menu">Cerrar Sesión</a>';
	}
	?>
	

		
</div>
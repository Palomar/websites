<div class="cuadro-post">
	<?php
	/*<header class="titulo-topic">Titulo
	</header>
	<div class="post">
		<div class="nick">Nick
		</div>
		<div class="puntos">- 11 puntos
		</div>
		<div class="post-msg">Texto random escrio para probar Texto random escrio para probar Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		</div>
		
	</div>
	
	<div class="post">
		<div class="nick">Nick
		</div>
		<div class="puntos">- 11 puntos
		</div>
		<div class="post-msg">Texto random escrio para probar Texto random escrio para probar Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		Texto random escrio para probar
		</div>
		
	</div>*/
	
	
	
	?>
	
	<div class="post">
		<div class="nick">Análisis de la web de la práctica 1
		</div>
	</div>
	<div class="post">
	<div class="puntos">Visibilidad del estado del sistema:
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Se ofrece al usuario mensajes que identifican en qué momento inicia sesión, cuándo tiene iniciada sesión, se le ruega su espera, se le comunican posibles errores en caso de errores de conexión SQL y se le pide que contacte con el Administrador en algunas ocasiones, de URL no encontradas e incluso si trata de manipular las variables del GET en la barra de navegación.

Se puede afirmar que hay una buena retroalimentación dentro de lo sencillo del diseño web.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Relación entre el sistema y el mundo real:</div>
	</div>
	<div class="post">
	<div class="post-msg">Puesto que se trata de una web con un componente básico de red social donde los usuarios se registran e interactúan, siendo su perfil el de exalumnos, es destacable el uso de un logotipo que recuerda la nube y asociado muy a menudo a redes sociales. Se han empleado colores rojos para enlaces que destacan el retorno a la página anterior y el de alertar si se es un usuario nuevo por dónde debería acceder si no tiene cuenta para iniciar sesión. A su vez se le da la opción de registro en el menú de navegación, incluyendo la versión de cerrar sesión en su lugar una vez logueados.
	La disposición de elementos sigue la lógica habitual: logotipo y mensaje de introducción en una cabecera que incorpora un buscador. Luego un menú de navegación y posteriormente una ubicación a la derecha para tener a mano el panel de inicio de sesión y visualización de perfil una vez logueados, mientras a la izquierda el bloque de contenido de foros donde se interactúa. Abajo se apuesta por un pie de información tal como se sigue en las convenciones habituales de una web.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Control y libertad del usuario:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Al ser una web muy minimalista, las posibilidades de deshacer no son precisamente abundantes pero sí se da libertad al usuario para por ejemplo volver al inicio de la web, la home, desde el propio menú de navegación, además de un enlace para retroceder a la lista de temas en caso de acceder a un tema concreto.

Por otro lado se ofrece al usuario la posibilidad de repetir la contraseña para evitar que comunique una contraseña errónea cuando se registra en la web, dándole control sobre ese elemento que introduce.

Sin embargo no existen capacidades de edición o de eliminación de los temas abiertos o de las respuestas realizadas en la red, con lo que se está mermando considerablemente esa libertad. Tampoco se le da un control al usuario tal como modificar su perfil para cambiar su contraseña o su nombre (ni por supuesto más datos que podrían ser de interés para el usuario).
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Consistencia y estándares:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se están siguiendo estándares tan básicos como solicitar nombre y contraseña, con repetición de ésta en el registro, empleando formularios de inputs apropiados (texto que se visualiza para el nombre, input de contraseña para tapar el texto frente a la pantalla).

Se emplea una nomenclatura entendible: “usuario”, “contraseña”, “registrarse”, “home”, etc. junto a iconos que representan el usuario o la contraseña en los formularios. Además se facilitan descripciones de placeholders que ayudan a entender qué tipo de texto contiene el campo a rellenar.

Se ha verificado que los validadores no le hacen saltar ningún error grave de HTML5 o CSS3, lo cual es buen señal.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Prevención de errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se han gestionado errores del usuario tal como que introduzca una contraseña incorrecta gracias a la doble verificación de campo, lanzando una alerta y bloqueando el envío. Se ha procedido a garantizar que los campos que son obligatorios se rellenan avisando al usuario tanto con un mensaje como marcando en color rojo el campo que se olvida.

Se hallan mensajes de error en caso de fallos de conexión con la Base de Datos, de consultas SQL incorrectas o no disponibles, mensajes tanto descriptivos como con código y rogando que contacte al administrador, etc. También se ha previsto la manipulación de la URL del navegador si se trata de acceder a una zona sin estar iniciada la sesión, si trata de incluir datos no válidos se le redirigirá e incluso se alertará con un mensaje crítico y su redirección (tratar de manipular la “?p” indebidamente por ejemplo).

Ahora mismo el error que se puede dar y no tiene solución, salvo volverse a registrar, es la recuperación de la contraseña en caso de olvido por parte del usuario ya que no podrá ni cambiarla ni recuperarla. Y además totalmente porque la BBDD tiene almacenada la contraseña en un hash sha512 por lo que iniciar sesión desde el formulario es imposible con ese dato.
	</div>
	</div>
	
	
	
	<div class="post">
	<div class="puntos">Reconocimiento antes que recuerdo:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Al ser una web minimalista con pocas opciones y con toda la información a pocos clicks, hace que el usuario pueda familiarizarse rápidamente. Tan sólo tiene que clickar en el nombre del hilo donde quiere leer y tan solamente tendrá que rellenar el campo de texto y clickar en el botón de enviar para contestar. Incluso si quiere abrir un hilo, directamente puede hacerlo desde la home previo inicio de sesión.

En este sentido la web peca de un fallo ya que requiere que el usuario inicie sesión siempre que visite la web si su sesión se cerró, no dando la posibilidad de guardarla en una galleta, motivo por el cual obliga a recordar sus datos de identificación para poder iniciar sesión. Con lo cual hablamos de un paso previo al click de abrir un tema, escribir en los campos y enviarlo. Hablamos pues de 3 pasos de distancia desde que se accede a la web sin sesión hasta que tenemos un tema abierto con nuestras ideas en la web.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Flexibilidad y eficiencia de uso:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Puesto que es una web muy básica, con pocos pasos en los que se interactúa, el verdadero problema de eficiencia en el uso se dará en el momento en que haya cientos de temas abiertos, que si bien se mostrará paginado y en orden descendente de acuerdo a la apertura realizada del tema, puede provocar claramente una gran dificultad de acceso a un tema pasado. Situación que se agrava con las respuestas ya que no tienen tampoco paginación de resultados.

La opción que se ha ofrecido para usuarios experimentados que quieran visitar temas pasados o indagar en la red, pasará por realizar consultas desde el buscador el cual filtra tanto por el nombre de tema como por las respuestas de tema, reflejando resultados no duplicados y ordenados por antigüedad.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Estética y diseño minimalista:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">La web cumple con una estética reducida de acuerdo a la función que pretende cumplir, con espacios planos que no distraen la vista, buscador y logotipos en la parte superior, contenido en la parte central y pie de página abajo siguiendo el formato clásico de una web.

La información es concisa e incluso la descripción de la web es corta tanto en una frase de gancho inicial junto al logotipo, como por la descripción de la página que explique qué es Alumni Network. Por otro lado los temas abiertos en el cuadro de contenido solamente muestran nombre de quién abre tema y su título, y en un tema salen nombre y mensaje enviado, así que el contenido queda enfocado a la comunicación entre los usuarios dejando de lado elementos que pueden provocar distracción (cálculo de mensajes, puntos, firmas, datos personales del usuario que envía mensaje, etc).

Quizá dicha estética pueda ser excesivamente minimalista si el usuario desease tener un grado de libertad para personalizarlo, así que desde ese punto de vista quizá peque de ser demasiado simple en estética.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Ayudar a los usuarios a reconocer, diagnosticar y recuperarse de los errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Los mensajes de error ofrecen una descripción clara y comprensible para el usuario, además de indicarle en ocasiones un código y su comunicación al Administrador. Se incide especialmente en la funcionalidad para redireccionar al usuario en caso de errores e incluso de mensajes de alertas cuando no se rellenan los campos obligatorios o se equivoca al rellenarlos.

Quizá el fallo más grave del cual no puede recuperarse un usuario es en el momento en que se registra en la web y se olvida posteriormente de la contraseña que introdujo ya que se provoca una situación en la que su cuenta quedará inutilizada, no será recuperable a menos que un administrador  borre su cuenta o manualmente ejecute un cambio de password.
	</div>
	</div>
	
	
	
	<div class="post">
	<div class="puntos">Ayuda y documentación:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">No existe ninguna ayuda ni PUF en la web. Aunque por la sencillez de la web es extremadamente intuitivo, sí que suspende en este aspecto por el hecho de no comunicar detalles tan relevantes como que el usuario puede no recuperar su cuenta en caso de perder su contraseña, algo que sería MUY importante comunicar al usuario que se registra.
	</div>
	</div>
	
	<div class="post">
		<div class="nick">Análisis de la web de la práctica 2
		</div>
	</div>
	
	<div class="post">
	<div class="puntos">Visibilidad del estado del sistema:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Mala visibilidad del estado del sistema por la disposición que ofrece, sin embargo facilita mensajes de logueo, despedida, etc. tal como se esperaría de una web usable. Aun así las redirecciones que se realizan son precisamente a la web usable por lo que también podría considerarse una web no usable en ese sentido al indicar mensajes en lugares no correctos.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Relación entre el sistema y el mundo real:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">La disposición de elementos es completamente desfigurada, con logotipos rotos y mal vinculados, con un video en bucle de fondo que no tiene absolutamente nada que ver y una molesta música de fondo que retira toda la inmersión posible que pudiera tener una web para exalumnos.

Se emplea una fuente en blanco que junto a los formularios en blanco impiden su lectura y su escritura, obligando al usuario a no poder comunicarse y relacionarse con los otros usuarios.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Control y libertad del usuario:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se da lugar a las mismas situaciones positivas y negativas que se daban en la primera web, con el añadido de que al tener un estilo completamente roto de la disposición de elementos, quizá se pueda considerar que se esté mermando la libertad del usuario desde el punto de vista en que se está obstruyendo su capacidad de navegación para poder acceder a la propia web, pues es más libre el usuario que puede hallar el formulario de logueo antes que aquel como en este caso se le puede provocar una situación en la que no sabe dónde se introduce su nombre y contraseña.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Consistencia y estándares:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">El validador lanzará un error en una etiqueta. No se respeta la ubicación natural de los espacios. Se emplean formatos de estilos (o la ausencia de éstos) que provoca lecturas imposibles, incómodas, no accesibles, imágenes en bucle de fondo, música de fondo, imágenes mal enlazadas, imposibilitando por ejemplo identificar bien los enlaces o dónde está el contenido de temas/respuestas, para qué sirven los campos de formularios, etc.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Prevención de errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Salvo por cuestiones relacionadas con el estilo, lo cierto es que aquí se trata la web de la misma forma que en el caso de la práctica 1, por lo que no hay nada que añadir.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Reconocimiento antes que recuerdo:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Es imposible reconocer exactamente qué es que por la disposición y colores empleados, con lo que se obliga al usuario a memorizar qué hace cada campo y dónde aparece cada elemento.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Flexibilidad y eficiencia de uso:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Carece de ningún acelerador, no es una web flexible ni puede ser eficiente su uso por lo mal distribuida que está, con un recarga innecesaria con la imagen de movimiento (gif) de fondo y el midi entorpecedor.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Estética y diseño minimalista:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Ciertamente sigue siendo minimalista porque es básica, pero la estética está completamente rota, inusable, y sigue conservando la misma cantidad de texto tal como la versión de la práctica 1.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Ayudar a los usuarios a reconocer, diagnosticar y recuperarse de los errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Sigue conservando los mismos mensajes que en la práctica 1, pero esta vez están ubicados de una forma incrustada junto al resto de texto por lo que resulta mucho más difícil que el usuario pueda darse cuenta de dichos mensajes de error y ayuda.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Ayuda y documentación:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Ninguna, tal como ocurre con la web de la práctica 1.
	</div>
	</div>
	
	
	<div class="post">
		<div class="nick">Análisis de la web realizada con ESDI
		</div>
	</div>
	
	<div class="post">
	<div class="puntos">Visibilidad del estado del sistema:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">No existen mensajes de acompañamiento durante el inicio de sesión, pero sí de posibles errores que se puedan producir tales como direcciones incorrectas que se procesen por el GET o conexiones mal realizadas con la BBDD que informan al usuario.

Se informa en la página central de contenido dónde está ubicado el usuario, aunque no hay atajos para retroceder al paso anterior.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Relación entre el sistema y el mundo real:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se emplean iconos identificativos para valorar por puntuaciones o para crear la lista de favoritos, se sigue un proceso de registro e inicio de sesión muy común para ello, el cual carece por otro lado de informaciones que indiquen qué es lo que puede estar mal introducido, de informar qué alergia ha sido seleccionada en el formulario.

Durante el proceso de selección de receta o restaurante lo hacemos por medio de un menú lateral que sigue un orden jerárquico asequible, y luego en la selección de la receta se nos ofrece una breve descripción al posicionarnos encima, una situación de información que es cada vez más común en los websites. Se ha procurado un buen uso de imágenes que identifican las recetas y los restaurantes, algo muy necesario y común en este tipo de webs, incluyendo datos tan importantes y lógicos como son los ingredientes junto a las descripciones de proceso de cocina.

Por otro lado también tiene un sistema de comentarios, lo cual es fundamental en un medio de recetas que pretende la interactuación de usuarios que puedan opinar y valorar.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Control y libertad del usuario:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">El control sobre la web es bastante amplio ya que el usuario puede valorar en cualquier momento las recetas y restaurantes, haciendo enmienda a su error. De la misma manera dicha situación se da al botón de corazón para añadir a favoritos ya que tendremos la posibilidad de activarlo o desactivarlo.

No se da libertad al usuario sin embargo para poder editar sus comentarios en la web, tampoco para editar su propio perfil y lo que es peor, no tiene posibilidad de recordar /recuperar la contraseña en caso de que se equivoque o la olvide.

Cuando se registra el usuario puede indicar a qué es alérgico, por lo que las recetas que estén relacionadas saldrán decoloradas en gris para indicarle que pueden no ser de su interés, si bien le da control sobre lo que quiere visualizar, lo malo es que como se ha dicho antes no se puede editar el perfil y todavía menos incluir más de un tipo de alergia.

El video de portada puede ser detenido a voluntad del usuario.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Consistencia y estándares:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se ha seguido el mismo estilo de estrellas y corazones para ofrecer ese tipo de funcionalidad en la web (puntuar y añadir a favoritos), mientras por otro lado se utilizó la convención de una lupa para indicar que allí se encuentra la búsqueda de datos, guiando además al usuario con sugerencias de búsqueda en función de aquello que introducía en el campo.

Se emplean enlaces que resaltan cuando se coloca encima como los dispuestos en la cabecera. Y aunque no se sigue un modelo habitual de menú ubicado en pantalla, si mantiene una estructura bastante limpia y usable con su cabecera superior para enlaces rápido, buscador y logueo, mientras hay un bloque de contenido intermedio para el cuadro de información de posición y un pie de página con la información propia.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Prevención de errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Se han gestionado errores de BBDD, no así errores de introducción de datos por parte del usuario que no podrá editar sus mensajes ni su perfil.

Se han gestionado errores de manipulación del GET de la url de la web, redireccionando a resultados válidos, los posibles escenarios que se den en el buscador como por ejemplo que no haya información por la cadena de texto a buscar o que haya muchas coincidencias.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Reconocimiento antes que recuerdo:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">El proceso de navegación es bastante guiado, intuitivo, ya que solamente se selecciona el tipo de categoría inicial de la cabecera (recetas, restaurantes o favoritos si se está logueado) y a través de ello el menú desplegado lateralmente nos indicará por ubicación y subcategoría por dónde queremos navegar.

El usuario no debe recordar cómo es la estructura del mapa ya que se reconoce fácilmente y además tiene el atajo de un buscador que le sugiere recetas y restaurantes que puede estar buscando. Los iconos usados tanto para interactuar como para el tooltip de información de ingredientes son sencillos y reconocibles.

Hallar el enlace para loguearse está en la parte superior derecha, muy fácil de reconocer.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Flexibilidad y eficiencia de uso:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Su mayor punto fuerte en cuanto a la eficiencia y flexibilidad para el usuario es incluir un buscador con sugerencias de búsquedas que permitirá a los usuarios avanzados tomar rutas rápidas sin tener que navegar por el menú de navegación que se desplega en el lateral izquierdo.

Iniciar y cerrar sesión está siempre a la vista en la parte superior así como el acceso a favoritos que está a tan solamente un click.

Acceder a una receta desde menú implica hacer 3 clicks siendo 2 de ellos dentro de la misma página, por lo que se cumple bastante bien el hecho de guardar la regla 7x3.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Estética y diseño minimalista:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">Las diseñadoras fueron muy insistentes en tener un diseño minimalista, de hecho si no fuera por los programadores la web habría sido mucho más plana, sin video y con un contenido menos coloreado.

Así que la visibilidad de la web mientras no estamos en una receta o restaurante es muy básica, limpia y procura no distraer la atención del usuario.

Quizá un defecto que se puede dar en cuanto la web tenga muchas recetas y restaurantes es que no existe paginación de resultados generando una web con demasiado scroll.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Ayudar a los usuarios a reconocer, diagnosticar y recuperarse de los errores:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">No existen muchos detalles sobre los errores producidos aunque se informa al usuario para los casos más graves como son la manipulación de enlaces o los errores de conexión con la BBDD. En dichos errores no se lleva a cabo ninguna redirección por lo general, así que no ayuda a recuperarse de los errores, y tan sólo se le redirecciona en casos de cadenas de manipulación del GET que son ajenas como en platos de sushi, que nos informa de que no hay resultados. Si la manipulamos dentro de una receta se nos mostrará la página pero vacía si introducimos un número no válido, sin informar al usuario del error. Si introducimos una cadena de texto si que nos redirigirá a la receta con id=1, pero no se le informa de dicha redirección.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Ayuda y documentación:</div>
	</div>
	
	<div class="post">
	<div class="post-msg">No existe ninguna.
	</div>
	</div>
	
</div>
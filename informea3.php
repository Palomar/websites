<div class="cuadro-post">
	
	<div class="post">
		<div class="nick">TEST 1 <br>Perfil: Adulto, con pocos conocimiento de navegabilidad web pero acostumbrado
		<br><a href="test1.ogg">test1.ogg</a>
		</div>
	</div>
	<div class="post">
	<div class="puntos">Dificultad para comprender la temática de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Reconoce la temática. <br>Tiempo: 10s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Dificultad de navegación y localización del punto de información explicativo de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No ve con claridad su navegabilidad y tampoco halla el punto de información (¿Qué somos?).
	<br>Tiempo: 15s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Se le invita a registrarse en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Trata de registrarse directamente rellenando la información en los campos en los primeros intentos.
	<br>Tiempo: 30s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Posteriormente encuentra el enlace "¿Eres nuevo?" y logra registrarse. No usa la opción "Registrarse" ubicada en el panel de menú.
	<br>Tiempo: 1m aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le invita a abrir un tema dentro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Encuentra el botón para abrir tema.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Sabe manejarse para escribir en el formulario y enviar el tema.
	<br>Tiempo: 20s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le invita a responder en el mismo tema o en otro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Halla la opción de responder fácilmente.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le indica que busque la opción de "Buscador" en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">La halla con facilidad.
	<br>Tiempo: 3s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Realiza una búsqueda sin mayor problema.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">También se le indica si encuentra una opción para retornar al punto inicial de temas una vez respondió
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Lo encuentra fácilmente.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Se le indica que cierre sesión
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Lo hace sin mayor problema.
	<br>Tiempo: 10s aprox.
	</div>
	</div>
	
	
	<div class="post">
		<div class="nick">TEST 2 <br>Perfil: Adulto, no acostumbrado a la navegación web.
		<br><a href="test2.ogg">test2.ogg</a>
		</div>
	</div>
	
	<div class="post">
	<div class="puntos">Dificultad para comprender la temática de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Reconoce la temática. 
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Dificultad de navegación y localización del punto de información explicativo de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No ve con claridad su navegabilidad, confunde el botón "Web no Usable" como aquel que le puede ofrecer la información de la web. 
	<br>Tiempo: 30s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Se le sugiere si está seguro y rectifica reconociendo que es en "¿Qué somos?". 
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Se le indica que acceda a ello y navegue por esa página. El usuario no reconoce cómo navegar hacia abajo para leer la información. La confunde incluso con el panel "iniciar sesión".
	<br>Tiempo: 10s aprox.
	</div>
	</div>

	<div class="post">
	<div class="puntos">Se le invita a registrarse en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Logra registrarse sin mayor problema. Accede desde la opción del panel de menú.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Tarda mucho tiempo en darse de alta y rellena los 3 formularios.
	<br>Tiempo: 2m aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le invita a abrir un tema dentro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Encuentra el botón para abrir tema.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Sabe manejarse para escribir en el formulario y enviar el tema aunque tarde tiempo en rellenar el formulario.
	<br>Tiempo: 2m aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le invita a responder en el mismo tema o en otro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No halla la opción para responder. Se le pregunta por dónde cree que está, el usuario espera una respuesta de alguien en su tema.
	<br>Tiempo: 40s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le indica que busque la opción de "Buscador" en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No halla el buscador y lo confunde con otros botones propios del navegador.
	<br>Tiempo: 1m aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Se le indica que cierre sesión
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No encuentra la opción para cerrar sesión y la confunde con "volver atrás", con el botón para cerrar el navegador y el de cerrar la pestaña del navegador.
	<br>Tiempo: 1m aprox.
	</div>
	</div>
	
	
	
	<div class="post">
		<div class="nick">TEST 3 <br>Perfil: Adulto, con amplios conocimiento de navegabilidad web y muy acostumbrado
		<br><a href="test3.ogg">test3.ogg</a>
		</div>
	</div>
	
	<div class="post">
	<div class="puntos">Dificultad para comprender la temática de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Reconoce la temática al instante definiéndola como "red social".
	<br>Tiempo: 2s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Dificultad de navegación y localización del punto de información explicativo de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Encuentra con facilidad el punto de información y accede a él, navegando por la página.
	<br>Tiempo: 3s aprox en hallarlo.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Se le invita a registrarse en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">El propio usuario por iniciativa propia se registra sin que le avisemos de que haga dicho paso, rellenando los formularios rápidamente. Le resulta fácil.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	
	
	<div class="post">
	<div class="puntos">Se le invita a abrir un tema dentro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Encuentra el botón para abrir tema rápidamente y escribe un tema sin dificultad.
	<br>Tiempo: 1s aprox. en hallarlo
	</div>
	</div>
	<div class="post">
	<div class="post-msg">Se le comenta si puede encontrar una opción para volver atrás junto al resto de temas a parte del botón de "home" y lo encuentra.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le invita a responder en el mismo tema o en otro de la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No tiene problemas para responder a un tema. La disposición de la web le resulta fácil.
	<br>Tiempo: 3s aprox.
	</div>
	</div>
	
	
	<div class="post">
	<div class="puntos">Se le indica que busque la opción de "Buscador" en la web
		</div>
	</div>
	<div class="post">
	<div class="post-msg">No tiene problemas destacables y encuentra el buscador y lo usa con naturalidad para hallar su propio tema.
	<br>Tiempo: 3s aprox.
	</div>
	</div>
	
	<div class="post">
	<div class="puntos">Se le indica que cierre sesión
		</div>
	</div>
	<div class="post">
	<div class="post-msg">Encuentra el botón para cerrar sesión, no tan rápidamente como en los anteriores pasos. Se le pregunta por la disposición de formularios para iniciar sesión y considera que son buenos.
	<br>Tiempo: 5s aprox.
	</div>
	</div>
</div>
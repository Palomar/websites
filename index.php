<?php
include('connection.php');
$rsPage = 10;
$start_from = 0;
if(isset($_GET['p'])){
	if (intval($_GET['p'] == 0)){
		header("Refresh: 5; url=index.php");
		die("No puedes manipular la url de registros.");
	}else{
		$p = intval($_GET['p']);
		if($p == 1){
			header("Location: index.php");
			die();
		}
		$start_from = ($p-1) * $rsPage;
	}
	
}else{
	$p = 1;
}
session_start();
//REGISTRO
if(isset($_POST['usuari']) && isset($_POST['password'])){
	header("Refresh: 5; url=index.php");
	
	$usuari = $_POST["usuari"];
	$password = hash("sha512",$_POST["password"],false);
	$consulta=sprintf("SELECT nombre FROM usuarios WHERE nombre='%s'",mysqli_real_escape_string($connexio, $usuari));
	$resultat=mysqli_query($connexio,$consulta);
	if(!$resultat){
		die ("No se pudo ejecutar la consulta SQL. ERROR 007, contacte con el Administrador.");
	}
	
	if (mysqli_num_rows($resultat) > 0){
		$registro = false;
		//header("Refresh: 3; url=index.php");
	}else{
		//Si no existe se crea
		
		$insert=sprintf("INSERT INTO usuarios (nombre,password) VALUES ('%s','%s')",
		mysqli_real_escape_string($connexio, $usuari),
		mysqli_real_escape_string($connexio, $password)
		);
		
		
		
		$registro = true;
		$_SESSION["usuario"] = $usuari;
		
		$resInsert=mysqli_query($connexio,$insert);
		if(!$resInsert){
			$registro = false;
			die ("No se pudo ejecutar la consulta SQL para insertar usuario");
		}
		//Prepara una ID nueva para la sesión
		$sql = "select max(id) as id from usuarios";
		$resId=mysqli_query ($connexio, $sql);
		if (!$resId) die ("ERROR: No se pudo ejecutar consulta SQL para crear una ID");
		$fila=mysqli_fetch_array ($resId);
		$id = $fila['id'];
		$_SESSION["id"] = $id;
		
	}
	
	
}else if(isset($_POST['user']) && isset($_POST['pass'])){
	//LOGUEO
	header("Refresh: 5; url=index.php");
	$usuari = $_POST["user"];
	$password = hash("sha512",$_POST["pass"],false);
	$consulta=sprintf("SELECT * FROM usuarios WHERE nombre='%s' AND password='%s'",mysqli_real_escape_string($connexio, $usuari),mysqli_real_escape_string($connexio, $password));
	$consultaNom=sprintf("SELECT * FROM usuarios WHERE nombre='%s'",mysqli_real_escape_string($connexio, $usuari));
	$resultat=mysqli_query($connexio,$consulta);
	$resultatNom=mysqli_query($connexio, $consultaNom);
	
	if(!$resultat || !$resultatNom){
		die ("No se pudo ejecutar la consulta SQL");
	}
	
	$login = false;
	$userOK = false;

	if (mysqli_num_rows($resultatNom) > 0){
		$userOK = true;
	}

	if (mysqli_num_rows($resultat) > 0){
		$login = true;
		$fila = mysqli_fetch_assoc($resultat);
		$id = $fila['id'];
	}
	
	if ($login){
		$_SESSION["usuario"] = $usuari;
		$_SESSION["id"] = $id;
	}
	
}

if(isset($_GET['logout']) && isset($_SESSION['usuario'])){
	//Cierra sesión
	session_destroy();
	header("Refresh: 5; url=index.php");
}

//PROCESA ENVIO DE TEMA

if(isset($_POST['tema']) && isset($_POST['comentario'])){
	$tema = $_POST['tema'];
	$comentario = $_POST['comentario'];
	
	
	$insertTopic=sprintf("INSERT INTO topics (tema,usuarios_id) VALUES ('%s','%s')",
	mysqli_real_escape_string($connexio, $tema),
	mysqli_real_escape_string($connexio, $_SESSION["id"])
	);
	$resultat=mysqli_query($connexio,$insertTopic);
	if(!$resultat){
		die ("No se pudo ejecutar la consulta SQL. ERROR 069, contacte con el Administrador.");
	}
	
	//Prepara una ID nuevo topic
	$sql = "select max(id) as id from topics";
	$resId=mysqli_query ($connexio, $sql);
	if (!$resId) die ("ERROR: No se pudo ejecutar consulta SQL para extraer ID topic");
	$fila=mysqli_fetch_array ($resId);
	$_SESSION['topic_id'] = $fila['id'];
	
	
	$insertPost=sprintf("INSERT INTO posts (descripcion,topics_id,usuarios_id) VALUES ('%s','%s','%s')",
	mysqli_real_escape_string($connexio, $comentario),
	mysqli_real_escape_string($connexio, $_SESSION['topic_id']),
	mysqli_real_escape_string($connexio, $_SESSION['id'])
	);
	$resultat=mysqli_query($connexio,$insertPost);
	if(!$resultat){
		die ("No se pudo ejecutar la consulta SQL. ERROR 666, contacte con el Administrador.");
	}
}else if(isset($_POST['respuesta']) && isset($_GET['t'])){
	//PROCESARÁ RESPUESTAS A UN TEMA
	if (intval($_GET['t'] == 0)){
		die("No puedes manipular la url de tema.");	
	}else{
		$t = intval($_GET['t']);
		$comentario = $_POST['respuesta'];
		
		$insertPost=sprintf("INSERT INTO posts (descripcion,topics_id,usuarios_id) VALUES ('%s','%s','%s')",
		mysqli_real_escape_string($connexio, $comentario),
		mysqli_real_escape_string($connexio, $t),
		mysqli_real_escape_string($connexio, $_SESSION['id'])
		);
		$resultat=mysqli_query($connexio,$insertPost);
		if(!$resultat){
			die ("No se pudo ejecutar la consulta SQL. ERROR 666, contacte con el Administrador.");
		}
	}
}else if(isset($_POST['search'])){
	$search = $_POST['search'];
}

include('header.php');
?>


<div class="contenido">
	<?php
		if(isset($_GET['t'])){
			//Carga página de un tema
			$topic = $_GET['t'];
			include('topic.php');
		}else if(isset($_GET['new']) && isset($_SESSION["usuario"])){
			//Carga página para crear un tema
			$newtopic = $_GET['new'];
			include('topic-open.php');
		}else if(isset($_GET['register'])){
			//Carga página de registro
			include('register.php');
		}else if(isset($_POST['usuari']) && isset($_POST['password']) && isset($registro)){
			if($registro){
				//Procesa correctamente el registro
				echo '<div class="msg-welcome center">Bienvenido '.$_POST['usuari'].', estás siendo redireccionado espera unos segundos...
				</div>';
			}else{
				//Error en el registro
				echo '<div class="msg-welcome center">Ya existe un usuario así lo sentimos, estás siendo redireccionado espera unos segundos...
				</div>';
			}
		}else if(isset($_GET['logout'])){
			//Despedida al desloguearse
			echo '<div class="msg-welcome center">Hasta la vista, estás siendo redireccionado espera unos segundos...
			</div>';
		}else if(isset($_GET['info'])){
			include('info.php');
		}else if(isset($_GET['informea4'])){
			include('informea4.php');
		}else if(isset($_GET['informea3'])){
			include('informea3.php');
		}else if(isset($search)){
			include('search-results.php');
		}else if(isset($login)){
			if($login){
				echo '<div class="msg-welcome center">Bienvenido '.$_SESSION['usuario'].', estás siendo redireccionado espera unos segundos...
				</div>';
			}else if($userOK && !$login){
				echo '<div class="msg-welcome center">Te equivocaste de contraseña, estás siendo redireccionado espera unos segundos...
				</div>';
			}else if(!$userOK){
				echo '<div class="msg-welcome center">No existe ese usuario, estás siendo redireccionado espera unos segundos...
				</div>';
			}
			
		}else{
			include('listopics.php');
		}
	
	?>
	
	<?php
	if(isset($_GET['informea3'])){
		/*echo '<div class="form-style-9 ini-sesion">
				<p>Azul Verde Rojo Amarillo Marrón Gris Blanco
				</p>
		</div>';*/
		
		echo '<div class="cloud">
					<div class="puntos"><span class="cloud1">Confusa</span>
										<span class="cloud2">Sencilla</span>
										<span class="cloud2">Correcta</span>
										<span class="cloud2">Fácil</span>
										<span class="cloud2">Exalumnos</span>
										<span class="cloud4">Identificable</span> 
										<span class="cloud1">Red social</span>
										<span class="cloud3">¿Qué somos?</span>
										<span class="cloud2">Pestañas</span>
										<span class="cloud2">Volver Atrás</span>
										<span class="cloud1">Visible</span>
										<span class="cloud3">Registrarse</span>
										<span class="cloud1">Icono</span>
										<span class="cloud3">Tema</span>
										<span class="cloud2">Esquina derecha</span>
					</div>
				
		</div>';
	}else if(!isset($_GET['register']) && !isset($_POST['usuari']) && !isset($_SESSION["usuario"]) && !isset($_POST['user'])){	
		include('login.php');
	}else if(isset($_SESSION["usuario"]) && !isset($_GET['register']) && !isset($_POST['usuari']) && !isset($login) && !isset($_GET['logout'])){
		//Muestra mensaje bievenida en zona login
		echo '<div class="form-style-9 logintope ini-sesion ini-log">¡Bienvenido '.$_SESSION["usuario"].'!
				<p><img class="ico-login" src="img/icon.png" /></p>
		</div>';
	}
	
	?>
	
</div>
<?php
include('footer.php');
mysqli_close($connexio);
?>
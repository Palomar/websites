# Websites académicos

Los websites que se encuentran en este repositorio atienden a proyectos o prácticas realizadas a lo largo de una formación académica y/o con propósito de aprendizaje de diversas tecnologías webs.

## Práctica de web usable y no usable 

Práctica consistente en elaborar una web que cumpla parte de los estándares de usabilidad y otra basada en la misma que trate de no cumplir ninguno.

Se desarrolló teniendo en cuenta que requería de interacción con el usuario (registrarse, escribir mensajes,etc.), añadiendo los correspondientes informes de análisis y de test a tres usuarios.

Realizada en poco más que un fin de semana, incluyendo además parte de accesibilidad y adaptada a dispositivos móviles.

### Instrucciones de instalación

1. Ejecutar el archivo "usabilidadsql.sql" en el gestor MySQL para cargar la BBDD.
2. Subir todos los archivos del repositorio a la estructura del localhost donde se desea comprobar la funcionalidad. Ejecutar solamente en un entorno controlado para pruebas y estudio.
3. Editar el archivo "connection.php" y ajusta las variables $db_host, $db_usuari y $db_contrasenya de acuerdo a tus necesidades de entorno para poder conectar con la BBDD.
4. Abrir la página como [localhost]/[carpeta-donde-se-ha-copiado]/index.php

### Tecnologías usadas

* PHP5
* MySQL
* MySQL Workbench
* Notepad++
* HTML5 + CSS3/Flexbox

### Aviso y uso

Eres libre de usar como gustes el material de esta web. Ten en cuenta que forma parte de una práctica académica y por tanto no debería usarse en entorno de producción, por incompleto e inseguro. Existe mucho código basura comentado que proviene de otro proyecto previo, puede ignorarse o borrarse.

No me responsabilizo de los daños que puedan derivarse del mal uso intencionado o no.